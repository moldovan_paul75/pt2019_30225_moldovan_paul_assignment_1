package PT2019.demo.DemoProject;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import op.PolynomialOp;
import polynomial.IntegerMonom;
import polynomial.Polinom;

public class PolinomTest {
	@Test
	public void testConstructor() {
		Polinom p = new Polinom();
		Assert.assertEquals("empty", p.toString());
	}
	
	@Test
	public void testContructor2() {
		Polinom p = new Polinom("x^3-3x+1");
		Assert.assertEquals("x^3-3x+1", p.toString());
	}
	
	@Test
	public void addIntegerMonom() {
		Polinom p = new Polinom();
		p.addMonom(new IntegerMonom(3,2));
		Assert.assertEquals("3x^2", p.toString());
	}
	
	
	@Test
	public void isEmpty() {
		Polinom p = new Polinom();
		Assert.assertEquals(true, p.isEmpty());
		p.addMonom(new IntegerMonom(1,1));
		Assert.assertEquals(false, p.isEmpty());
	}
	
	@Test
	public void getSize() {
		Polinom p = new Polinom("3x^2-3x+1");
		Assert.assertEquals(3, p.getSize());
	}
	
	@Test
	public void getMonom() {
		Polinom p = new Polinom("3x^2-3x+1");
		Assert.assertEquals("-3x", p.getMonom(1).toString());
	}


}
