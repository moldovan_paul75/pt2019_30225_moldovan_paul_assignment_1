package PT2019.demo.DemoProject;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import op.PolynomialOp;
import polynomial.Polinom;

public class OpTest {

	@Test
	public void testAdd() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("10x^3-3x^2-22-3x");
		Polinom p2 = new Polinom("x^2-3x+10-20x^-2");
		Assert.assertEquals("10x^3-2x^2-6x-12-20x^(-2)", o.add(p1,p2).toString());
	}
	
	@Test
	public void testSub() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("10x^3-3x^2-22-3x");
		Polinom p2 = new Polinom("x^2-3x+10-20x^-2");
		Assert.assertEquals("10x^3-4x^2-32+20x^(-2)", o.sub(p1,p2).toString());
	}
	
	@Test
	public void testMul() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("10x^3-3x^2-22-3x");
		Polinom p2 = new Polinom("x^2-3x+10-20x^-2");
		Assert.assertEquals("10x^5-33x^4+106x^3-43x^2-164x-160+60x^(-1)+440x^(-2)", o.mul(p1,p2).toString());
	}
	
	@Test
	public void testDiv() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("3x^4-4x^3+12x^2+5");
		Polinom p2 = new Polinom("x^2+1");
		Assert.assertEquals("3x^2-4x+9+(4x-4)/(x^2+1)", o.div(p1,p2).toString());
	}
	
	@Test
	public void testInt() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("10x^3-3x^2-22-3x");
		Assert.assertEquals("(5/2)x^4-x^3(-3/2)x^2(-22/1)x", o.integration(p1).toString());
	}
	
	@Test
	public void testDeriv() {
		PolynomialOp o = new PolynomialOp();
		Polinom p1 = new Polinom("10x^3-3x^2-22-3x");
		Assert.assertEquals("30x^2-6x-3", o.derivation(p1).toString());
	}

}
