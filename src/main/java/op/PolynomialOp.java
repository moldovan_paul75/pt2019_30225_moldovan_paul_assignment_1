package op;

import polynomial.FloatMonom;
import polynomial.IntegerMonom;
import polynomial.Monom;
import polynomial.Polinom;
/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class PolynomialOp {
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * adunare v1
	 */
	public Polinom add(Polinom p1, Polinom p2) {
		
		p1.sortPolinom();
		p2.sortPolinom();
		
		Polinom finalP = new Polinom();
		//finalP.addMonom(new IntegerMonom(0,0));
		
		int i=0, j=0, p1Size = p1.getSize(), p2Size = p2.getSize();
		
		while(i < p1Size && j < p2Size) {
			if(p1.getMonom(i).getPower() > p2.getMonom(j).getPower()) {
				finalP.addMonom(p1.getMonom(i));
				i++;
			}
			if(p1.getMonom(i).getPower() < p2.getMonom(j).getPower()) {
				finalP.addMonom(p2.getMonom(j));
				j++;
			}
			if(p1.getMonom(i).getPower() == p2.getMonom(j).getPower()) {
				if(p1.getMonom(i).getCoef().intValue() + p2.getMonom(j).getCoef().intValue() != 0) {
					finalP.addMonom(new IntegerMonom(p1.getMonom(i).getCoef().intValue() + p2.getMonom(j).getCoef().intValue(), p1.getMonom(i).getPower()));
					i++; j++;
				}else {i++; j++;}
			}
		}
		while(i < p1Size) {
			finalP.addMonom(p1.getMonom(i));
			i++;
		}
		while(j < p1Size) {
			finalP.addMonom(p2.getMonom(j));
			j++;
		}
		return finalP;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * adunare v2
	 */
	public Polinom add2(Polinom p1, Polinom p2) {
		
		Polinom finalP = new Polinom(p1.toString());
		
		for(Monom m : p2.getPolinom()) {
			finalP = addMonom(finalP, m);
		}
		
		return finalP;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * scadere v1
	 */
	public Polinom sub(Polinom p1, Polinom p2) {
		
		p1.sortPolinom();
		p2.sortPolinom();
		
		Polinom finalP = new Polinom();
		
		int i=0, j=0, p1Size = p1.getSize(), p2Size = p2.getSize();
		
		while(i < p1Size && j < p2Size) {
			if(p1.getMonom(i).getPower() > p2.getMonom(j).getPower()) {
				finalP.addMonom(p1.getMonom(i));
				i++;
			}
			if(p1.getMonom(i).getPower() < p2.getMonom(j).getPower()) {
				finalP.addMonom(new IntegerMonom(-p2.getMonom(j).getCoef().intValue(), p2.getMonom(j).getPower()));
				j++;
			}
			if(p1.getMonom(i).getPower() == p2.getMonom(j).getPower()) {
				if(p1.getMonom(i).getCoef().intValue() - p2.getMonom(j).getCoef().intValue() != 0) {
					finalP.addMonom(new IntegerMonom(p1.getMonom(i).getCoef().intValue() - p2.getMonom(j).getCoef().intValue(), p1.getMonom(i).getPower()));
					i++; j++;
				} else {i++; j++;}
			}
		}
		while(i < p1Size) {
			finalP.addMonom(p1.getMonom(i));
			i++;
		}
		while(j < p1Size) {
			finalP.addMonom(new IntegerMonom(-p2.getMonom(j).getCoef().intValue(), p2.getMonom(j).getPower()));
			j++;
		}
		return finalP;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * scadere v2
	 */
	public Polinom sub2(Polinom p1, Polinom p2) {
		
		Polinom finalP = new Polinom(p1.toString());
		
		for(Monom m : p2.getPolinom()) {
			finalP = addMonom(finalP, new IntegerMonom(-m.getCoef().intValue(), m.getPower()));
		}
		
		return finalP;
	}
	
	/**
	 * @param p
	 * @param m
	 * @return
	 * adaugare monom la polinom
	 */
	public Polinom addMonom(Polinom p, Monom m) {
		
		Polinom finalP = new Polinom();
		int i=0, power = m.getPower(), coef = m.getCoef().intValue();
		
		for(Monom m1 : p.getPolinom()) {
			if(m1.getPower() == power){
				finalP.addMonom(new IntegerMonom(m1.getCoef().intValue()+coef, power));
			}else {
				finalP.addMonom(new IntegerMonom(m1.getCoef().intValue(), m1.getPower()));
				i++;
			}
		}
		if(i == p.getSize()) {
			finalP.addMonom(new IntegerMonom(coef, power));
		}

		return finalP;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * inmultire
	 */
	public Polinom mul(Polinom p1, Polinom p2) {
		
		Polinom finalP = new Polinom();
		int coef=0, power=0;
		
		for(Monom m1 : p1.getPolinom()) {
			coef = m1.getCoef().intValue();
			power = m1.getPower();
			
			for(Monom m2 : p2.getPolinom()) {
				finalP = addMonom(finalP, new IntegerMonom(coef*m2.getCoef().intValue(), power + m2.getPower()));
			}
		}
		
		return finalP;
	}
	
	
	/**
	 * @param p
	 * @param m
	 * @return
	 * inmultire polinom cu monom
	 */
	public Polinom mulMonom(Polinom p, Monom m) {
		
		Polinom finalP = new Polinom();
		float coef = m.getCoef().floatValue();
		int power = m.getPower();
		
		for(Monom m1 : p.getPolinom()) {
			finalP.addMonom(new FloatMonom(m1.getCoef().floatValue()*coef, m1.getPower()+power));
		}
		
		return finalP;
	}
	
	/**
	 * @param p
	 * @return
	 * derivare
	 */
	public Polinom derivation(Polinom p) {
		
		Polinom finalP = new Polinom();
		int power = 0;
		for(Monom m : p.getPolinom()) {
			power = m.getPower();
			if(power != 0) finalP.addMonom(new IntegerMonom(m.getCoef().intValue()*power, power-1));
		} 
		
		return finalP;
	}
	
	/**
	 * @param p
	 * @return
	 * integrare
	 */
	public Polinom integration(Polinom p) {
		
		Polinom finalP = new Polinom();
		int power = 0;
		for(Monom m : p.getPolinom()) {
			power = m.getPower();
			finalP.addMonom(new FloatMonom((float)m.getCoef().intValue()/(power+1), power+1));
		}
		
		return finalP;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @return
	 * impartire long divison
	 */
	public String div(Polinom p1, Polinom p2) {
		
		Polinom finalP = new Polinom();
		Polinom aux = new Polinom(p1.toString());
		Polinom aux2 = new Polinom();
		float coef = 0;
		int power = 0;
	
		while(!aux.isEmpty() && aux.getMonom(0).getPower() >= p2.getMonom(0).getPower()) {
			
			coef = aux.getMonom(0).getCoef().floatValue()/p2.getMonom(0).getCoef().floatValue();
			power = aux.getMonom(0).getPower() - p2.getMonom(0).getPower();
			

			aux2 = mulMonom(p2, new FloatMonom(coef, power));
			aux = sub2(aux, aux2); 
			
			finalP = addMonom(finalP,  new FloatMonom(coef, power));
			
//			System.out.println("aux2 :" +aux2);
//			System.out.println("aux :" +aux);
//			System.out.println("final :" +finalP);
		}
		return finalP + "+(" + aux + ")/" + "(" + p2 + ")";
	}
}