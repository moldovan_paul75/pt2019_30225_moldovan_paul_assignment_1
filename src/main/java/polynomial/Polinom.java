package polynomial;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class Polinom{
	
	/**
	 * lista monoame
	 */
	private List<Monom> polinom;
	
	/**
	 * constructor polinom
	 */
	public Polinom() {
		polinom = new ArrayList<Monom>();
	}
	
	/**
	 * @param input
	 * polinom din string
	 */
	public Polinom(String input) {
		polinom = new ArrayList<Monom>();
		
		String[] arr = input.replace("x+","x^1+").replace("x-","x^1-").replace("-x","-1x").replace("+x","+1x").replace("-","+-").replace("^+-","^-").split("\\+");
		Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
		
		for(String part : arr){
			if(part.matches("-?\\d+(\\.\\d+)?")) part = part+"x^0";
			if(part.endsWith("x")) part = part+"^1";
			if(part.startsWith("x")) part = "1"+part;
			Matcher m = p.matcher( part );
				while (m.find()) {
				    //System.out.println("Coef: " + m.group(1));
				    //System.out.println("Degree: " + m.group(2));
					polinom.add(new IntegerMonom(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))));
				}
			}
		Collections.sort(polinom);
	}
	
	/**
	 * @return lista de monoame(arraylist)
	 */
	public ArrayList<Monom> getPolinom() {
		return (ArrayList<Monom>) polinom;
	}
	
	/**
	 * @param monom
	 * metoda pentru adaugarea unui monom nou in lista
	 */
	public void addMonom(Monom monom) {
		polinom.add(monom);
	}
	
	/**
	 * @return
	 * metoda pentru verificare daca polinomul este gol
	 */
	public boolean isEmpty() {
		if(polinom.isEmpty()) return true;
		else return false;
	}
	
	/**
	 * sortare polinom in functie de grade
	 */
	public void sortPolinom() {
		Collections.sort(polinom);
	}
	
	/**
	 * @return nr de monoame din lista
	 */
	public int getSize() {
		return polinom.size();
	}
	
	/**
	 * @param i
	 * @return monomul de la indexul i
	 */
	public Monom getMonom(int i) {
		return polinom.get(i);
	}
	
	/**
	 * @param i
	 * @return monomul cu coeficient negat
	 */
	public Monom negMonom(int i) {
		Monom m = getMonom(i);
		m.setCoef(-m.getCoef().floatValue());
		return m;
	}
	
	/**
	 * @return verifica daca polinomul este este gol
	 */
	public boolean getZero() {
		int z=0;
		int n = polinom.size();
		if(n > 0) {
			for(int i=0; i<n; i++)
				if(polinom.get(i).getCoef().intValue() == 0) z++;
		
			if(z == n) return true;
			else return false;
		}else return false;
	}
	
	/**
	 * sterge monoamele cu coeficient 0
	 */
	public void removeZero() {
		
		for(int i=0; i<polinom.size(); i++)
			if(polinom.get(i).getCoef().floatValue() == 0) polinom.remove(i);
	}
	
	/**
	 * goleste tot polinomul
	 */
	public void clearPolinom() {
		for(int i=0; i<polinom.size(); i++) {
			polinom.remove(i);
		}
	}
	
	@Override
	public String toString() {
		
		if(this.getZero()) return "";
		
		if(!polinom.isEmpty()) {
			
			Collections.sort(polinom);
			
			this.removeZero();
			
			String finalP = "";	
			
			for(Monom monom : polinom) {
				//if(monom.getCoef().floatValue() != 0) {
					if(finalP != "" && (polinom.indexOf(monom) < polinom.size()) && monom.getCoef().floatValue() > 0) finalP += "+";
					
					finalP = finalP + monom.toString(); //String.valueOf(monom);
				//}
			}
			return finalP;
		}else return "empty";
	}
	
	
}
