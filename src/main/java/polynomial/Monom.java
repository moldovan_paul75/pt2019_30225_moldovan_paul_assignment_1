package polynomial;

/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public abstract class Monom implements Comparable<Monom>{
	
	/**
	 * @param gradul monomului
	 */
	protected int power;
	
	/**
	 * @param power
	 * setter grad
	 */
	public void setPower(int power) {
		this.power = power;
	}
	
	/**
	 * @return gradul monomului
	 */
	public int getPower() {
		return this.power;
	}

	public int compareTo(Monom o) {
		return (int)(o.getPower() - this.getPower());
	}
	
	/**
	 * @param coef
	 * setter coeficient
	 */
	public abstract void setCoef(Number coef);
	
	/**
	 * @return coeficient
	 */
	public abstract Number getCoef();
	
	public abstract String toString();
	
}
