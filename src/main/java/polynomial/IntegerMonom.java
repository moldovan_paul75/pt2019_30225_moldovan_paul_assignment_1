package polynomial;

public class IntegerMonom extends Monom{
	
	private Integer coef;
	
	/**
	 * @param coef
	 * @param power
	 * constructor monom intreg
	 */
	public IntegerMonom(Integer coef, int power) {
		this.coef = coef;
		this.power = power;
	}
	
	@Override
	public void setCoef(Number coef) {
		this.coef = (Integer) coef;
	}

	@Override
	public Integer getCoef() {
		return this.coef;
	}

	@Override
	public String toString() {
		String monomPrint = "";
		
		if (this.getCoef() == 0) return "";
		else {
			if(this.getPower() != 0) {
				if(this.getCoef() != 1 ) { 
					if(this.getCoef() != -1) monomPrint += this.getCoef() + "";
					else monomPrint += "-";
				}
				if(this.getPower() == 1) monomPrint += "x";
				else {
					if(this.getPower() > 1) monomPrint += "x^" + this.getPower();
					else monomPrint += "x^(" + this.getPower() + ")";
				}
			}
			else {
				if(this.getCoef() != -1) monomPrint += this.getCoef() + "";
				else monomPrint += "-1";
			}
		}
		return monomPrint;
	}
}
