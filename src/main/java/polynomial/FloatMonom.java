package polynomial;

/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class FloatMonom extends Monom{
	
	private Float coef;
	
	/**
	 * @param coef
	 * @param power
	 * constructor monom real
	 */
	public FloatMonom(Float coef, int power) {
		this.coef = coef;
		this.power = power;
	}

	@Override
	public void setCoef(Number coef) {
		this.coef = (Float) coef;
	}

	@Override
	public Float getCoef() {
		return this.coef;
	}
	
	/**
	 * @param x
	 * @return
	 * metoda folosita pentru conversie in fractie
	 */
	static private String convertDecimalToFraction(double x){
	    if (x < 0){
	        return "-" + convertDecimalToFraction(-x);
	    }
	    double tolerance = 1.0E-6;
	    double h1=1; double h2=0;
	    double k1=0; double k2=1;
	    double b = x;
	    do {
	        double a = Math.floor(b);	
	        double aux = h1; h1 = a*h1+h2; h2 = aux;
	        aux = k1; k1 = a*k1+k2; k2 = aux;
	        b = 1/(b-a);
	    } while (Math.abs(x-h1/k1) > x*tolerance);

	    return (int)h1+"/"+(int)k1;
	}
	
	@Override
	public String toString() {
		String monomPrint = "";
		
		if (this.getCoef() == 0) return "";
		else {
			if(this.getPower() != 0) {
				if(this.getCoef() != 1) { 
					if(this.getCoef() != -1) monomPrint += "(" + convertDecimalToFraction(this.getCoef()) + ")";
					else monomPrint += "-";
				}
				if(this.getPower() == 1) monomPrint += "x";
				else {
					if(this.getPower() > 1) monomPrint += "x^" + this.getPower();
					else monomPrint += "x^(" + this.getPower() + ")";
				}
			}
			else {
				if(this.getCoef()!=-1) monomPrint += "(" + convertDecimalToFraction(this.getCoef()) + ")";
				else monomPrint += "-1";
			}
		}
		return monomPrint;
	}
	
	
}
