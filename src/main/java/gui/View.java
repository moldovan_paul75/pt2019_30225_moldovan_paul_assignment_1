package gui;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class View extends JFrame{
	
    private JTextField input1 = new JTextField(5);
    private JTextField input2 = new JTextField(5);
    private JTextField result = new JTextField(20);
    private JButton	mulBtn = new JButton("Muliply");
    private JButton	addBtn = new JButton("Add");
    private JButton	subBtn = new JButton("Subtract");
    private JButton	derBtn = new JButton("Differentiate");
    private JButton	intBtn = new JButton("Integrate");
    private JButton divBtn = new JButton("Divide");
    private JButton	clearBtn = new JButton("Clear");
	
    private Model model;
    
    /**
     * @param model
     * constructor view
     */
    View(Model model){
    	
    	this.model = model;
    	
    	//main window
    	JFrame window = new JFrame();
    	window.setTitle("Calculator");
    	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	window.setSize(780, 320);
    	window.setResizable(false);
    	window.setVisible(true);
    	window.setLocation(780, 220);
    	
    	//panels
        JPanel content1 = new JPanel();
        JPanel content2 = new JPanel();
        JPanel content3 = new JPanel();
        JPanel content4 = new JPanel();
        JPanel p = new JPanel();
        JPanel p2 = new JPanel();
        
        //panels layout
        content1.setLayout(new FlowLayout());
        content2.setLayout(new FlowLayout());
    	content3.setLayout(new FlowLayout());  
    	content4.setLayout(new BoxLayout(content4, BoxLayout.Y_AXIS)); 
    	
    	p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS)); 
    	p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS)); 
    	
    	//buttons dimension
    	addBtn.setPreferredSize(new Dimension(120,30));
    	subBtn.setPreferredSize(new Dimension(120,30));
    	mulBtn.setPreferredSize(new Dimension(120,30));
    	divBtn.setPreferredSize(new Dimension(120,30));
    	intBtn.setPreferredSize(new Dimension(120,30));
    	derBtn.setPreferredSize(new Dimension(120,30));
    	clearBtn.setPreferredSize(new Dimension(120,30));
    	
    	result.setEditable(false);
    	
    	//buttons panel1
        content1.add(addBtn);
        content1.add(subBtn);
        content1.add(mulBtn);
        
        //buttons panel2
        content2.add(intBtn);
        content2.add(derBtn);
        content2.add(divBtn);
        
        //buttons
        p2.add(content1);
        p2.add(content2);
        
        //result panel
        content3.add(new JLabel("Result: "));
        content3.add(result);
        content3.add(clearBtn);
        
        //input fields
        content4.add(input1);
        content4.add(input2);
        
        //final
        p.add(p2);
        p.add(content4);
        p.add(content3);
        
        
        window.setContentPane(p);
        window.pack();
    }
    
    /**
     * reset
     */
    void reset() {
    	result.setText("");
    }
    
    /**
     * @return input field1
     */
    String getInput1() {
    	return input1.getText();
    }
    
    /**
     * @return input field 2
     */
    String getInput2() {
    	return input2.getText();
    }
    
    /**
     * @param newResult
     * set rezultat
     */
    void setResult(String newResult) {
    	result.setText(newResult);
    }
    
    /**
     * @param errMessage
     * afisare erroare
     */
    void showError(String errMessage) {
    	JOptionPane.showMessageDialog(this, errMessage);
    }
    
    /**
     * @param add
     * listener adunare
     */
    void addAdditionListener(ActionListener add) {
        addBtn.addActionListener(add);
    }
    
    /**
     * @param sub
     * listenere scadere
     */
    void addSubtractListener(ActionListener sub) {
    	subBtn.addActionListener(sub);
    }
    
    /**
     * @param mul
     * listener inmultire
     */
    void addMultiplyListener(ActionListener mul) {
        mulBtn.addActionListener(mul);
    }
    
    /**
     * @param div
     * listener impartire
     */
    void addDivisionListener(ActionListener div) {
    	divBtn.addActionListener(div);
    }
    
    /**
     * @param der
     * listener derivare
     */
    void addDerivationListener(ActionListener der) {
    	derBtn.addActionListener(der);
    }
    
    /**
     * @param in
     * listener integrare
     */
    void addIntegrationListener(ActionListener in) {
    	intBtn.addActionListener(in);
    }
    
    /**
     * @param clr
     * listener clear
     */
    void addClearListener(ActionListener clr) {
        clearBtn.addActionListener(clr);
    }
    
}
