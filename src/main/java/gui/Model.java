package gui;

import op.PolynomialOp;
import polynomial.Polinom;
/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class Model {
	
	 /**
	 * rezultatul operatiei
	 */
	private Polinom result = new Polinom();
	 /**
	 * obiect de tip operatie
	 */
	private PolynomialOp op = new PolynomialOp();
	 
	 /**
	 * contrusctor model
	 */
	Model(){
		 reset();
	 }
	 
	 /**
	 * reset polinom
	 */
	public void reset() {
		 result.clearPolinom();
	 }
	 
	 /**
	 * @param p1
	 * @param p2
	 * adunare
	 */
	public void add(String p1, String p2) {
		 this.reset();
		 result = op.add2(new Polinom(p1), new Polinom(p2));
	 }
	 
	 /**
	 * @param p1
	 * @param p2
	 * scadere
	 */
	public void sub(String p1, String p2) {
		 this.reset();
		 result = op.sub2(new Polinom(p1), new Polinom(p2));
	 }
	 
	 /**
	 * @param p1
	 * @param p2
	 * inmultire
	 */
	public void mul(String p1, String p2) {
		 this.reset();
		 result = op.mul(new Polinom(p1), new Polinom(p2));
	 }
	 
	 /**
	 * @param p1
	 * @param p2
	 * @return
	 * impartire
	 */
	public String div(String p1, String p2) {
		 this.reset();
		 return op.div(new Polinom(p1), new Polinom(p2));
	 }
	 
	 /**
	 * @param p
	 * derivare
	 */
	public void derivation(String p) {
		 this.reset();
		 result = op.derivation(new Polinom(p));
	 }
	 
	 /**
	 * @param p
	 * integrare
	 */
	public void integration(String p) {
		 this.reset();
		 result = op.integration(new Polinom(p));
	 }
	 
	 /**
	 * @param p
	 * set value rezultat
	 */
	public void setValue(String p) {
		 result = new Polinom(p);
	 }
	 
	 /**
	 * @return
	 * get value rezultat
	 */
	public String getValue() {
		 return result.toString();
	 }
}
