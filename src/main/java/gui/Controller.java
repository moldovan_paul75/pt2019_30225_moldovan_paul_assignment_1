package gui;

import java.awt.event.*;
/**
 * @author Moldovan Paul
 * @group 30225
 *
 */
public class Controller {
	
	/**
	 * model
	 */
	private Model model;
	/**
	 * view
	 */
	private View view;
	
	/**
	 * @param model
	 * @param view
	 * constructor controller
	 */
	Controller(Model model, View view){
		this.model = model;
		this.view = view;
		
		view.addAdditionListener(new AdditionListener());
		view.addSubtractListener(new SubtractListener());
		view.addMultiplyListener(new MultiplyListener());
		view.addDerivationListener(new DerivationListener());
		view.addIntegrationListener(new IntegrationListener());
		view.addDivisionListener(new DivisionListener());
		view.addClearListener(new ClearListener());
	}
	/**
	 * 
	 * listener pentru adunare
	 *
	 */
	class AdditionListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInput1 = "";
            String userInput2 = "";

                userInput1 = view.getInput1();
                userInput2 = view.getInput2();
                model.add(userInput1, userInput2);
                view.setResult(model.getValue());
                
        }	
	}
	
	/**
	 * 
	 * listener pentru scadere
	 *
	 */
	class SubtractListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInput1 = "";
            String userInput2 = "";
            
                userInput1 = view.getInput1();
                userInput2 = view.getInput2();
                model.sub(userInput1, userInput2);
                view.setResult(model.getValue());
                
        }
	}
	/**
	 * 
	 * listener pentru inmultire
	 *
	 */
    class MultiplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput1 = "";
            String userInput2 = "";
            
                userInput1 = view.getInput1();
                userInput2 = view.getInput2();
                model.mul(userInput1, userInput2);
                view.setResult(model.getValue());
                
        }
    }
	/**
	 * 
	 * listener pentru impartire
	 *
	 */
    class DivisionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput1 = "";
            String userInput2 = "";
            
                userInput1 = view.getInput1();
                userInput2 = view.getInput2();
                
                view.setResult(model.div(userInput1, userInput2));
                
        }
    }
	/**
	 * 
	 * listener pentru derivare
	 *
	 */
    class DerivationListener implements ActionListener{
    	public void actionPerformed(ActionEvent e) {
            String userInput1 = "";
   
                userInput1 = view.getInput1();
                model.derivation(userInput1);
                view.setResult(model.getValue());
                
        }
     }
    
	/**
	 * 
	 * listener pentru integrare
	 *
	 */
    class IntegrationListener implements ActionListener{
    	public void actionPerformed(ActionEvent e) {
            String userInput1 = "";

                userInput1 = view.getInput1();
                model.integration(userInput1);
                view.setResult(model.getValue());
                
   		}
     }
    
	/**
	 * 
	 * listener pentru clear
	 *
	 */
    class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	
            model.reset();
            view.reset();
        }
    }
    
}
